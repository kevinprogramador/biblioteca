<?php

namespace Entidades;

class Usuario {

    private $nombre;
    private $password;

    function __construct($nombre, $password) {
        $this->nombre = $nombre;
        $this->password = $password;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getPassword() {
        return $this->password;
    }
}
?>

