<?php
include 'Entidades/Usuario.php';
session_start();

if (!isset($_SESSION["user"])) {

    header("Location: ./login.php");
    exit();
} else {
    $usuario = $_SESSION["user"];
}
?>
<!doctype html>
    <?php
        include 'php/header.php';
        ?>
    <body class="container-fluid">
            <h1 class="display-4 text-center mt-5 mb-5">Mantenedor De Libros</h1>

            <?php
            //Conexion con la base
            include 'Negocio/Libros.php';

            $libro = new Negocio\Libros();

            $libros = $libro->obtenerListaLibros();
            ?>

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">NOMBRE LIBRO</th>
                        <th scope="col">AUTOR</th>
                        <th scope="col">EDITORIAL</th>
                        <th scope="col">AÑO</th>
                        <th scope="col">ESTADO</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (sizeof($libros) > 0) {

                        foreach ($libros as $_libro) {

                            echo '<tr><td>' . $_libro->getId() . '</td>';
                            echo '<td>' . $_libro->getNombrelibro() . '</td>';
                            echo '<td>' . $_libro->getAutor() . '</td>';
                            echo '<td>' . $_libro->getEditorial() . '</td>';
                            echo '<td>' . $_libro->getAnio() . '</td>';
                            echo '<td>' . $_libro->getEstado() . '</td>';
                        }
                    } else {
                        echo '<tr><td colspan="2">No existen registros.</td></tr>';
                    }
                    ?>
                </tbody>
            </table>
            <div class="row text-center">
                <div class="col-md-4">
                    <a href="insertar.php" class="btn btn-success boton">AÑADIR LIBRO</a><br>
                </div>
                <div class="col-md-4">
                    <a href="actualizar.php" class="btn btn-primary boton">ACTUALIZAR LIBRO</a><br>
                </div>
                <div class="col-md-4">
                    <a href="borrar.php" class="btn btn-danger boton">ELIMINAR LIBRO</a><br>
                </div>
            </div>
            
        </div>
        <br><br/><br/><br/><br/><br/><br><br/><br/><br/><br/><br/>
        <?php
        include 'php/footer.php';
        ?>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>

