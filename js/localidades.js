var Regiones = [
    {"id_region": "02", "nombre": "Metropolitana (RM)"},
    {"id_region": "03", "nombre": "Valparaiso (V)"},
    {"id_region": "04", "nombre": "O'Higgins (VI)"},
]

var Provincias = [
    {"id_region": "02", "id_provincia": "021", "nombre": "Chacabuco"},
    {"id_region": "02", "id_provincia": "022", "nombre": "Cordillera"},
    {"id_region": "02", "id_provincia": "023", "nombre": "Maipo"},
    {"id_region": "02", "id_provincia": "024", "nombre": "Santiago"},

    {"id_region": "03", "id_provincia": "031", "nombre": "Quillota"},
    {"id_region": "03", "id_provincia": "032", "nombre": "San Antonio"},
    {"id_region": "03", "id_provincia": "033", "nombre": "San Felipe"},
    {"id_region": "03", "id_provincia": "034", "nombre": "Valparaiso"},

    {"id_region": "04", "id_provincia": "041", "nombre": "Rancagua"},
    {"id_region": "04", "id_provincia": "042", "nombre": "Codegua"},
]

var Comunas = [
    {"id_region": "02", "id_provincia": "021", "id_comuna": "02101", "nombre": "Lampa"},
    {"id_region": "02", "id_provincia": "021", "id_comuna": "02102", "nombre": "Til Til"},

    {"id_region": "02", "id_provincia": "022", "id_comuna": "02201", "nombre": "Puente Alto"},
    {"id_region": "02", "id_provincia": "022", "id_comuna": "02202", "nombre": "San Jose De Maipo"},
    {"id_region": "02", "id_provincia": "022", "id_comuna": "02203", "nombre": "Pirque"},

    {"id_region": "02", "id_provincia": "023", "id_comuna": "02301", "nombre": "San Bernardo"},
    {"id_region": "02", "id_provincia": "023", "id_comuna": "02302", "nombre": "Calera de Tango"},
    {"id_region": "02", "id_provincia": "023", "id_comuna": "02303", "nombre": "Buin"},
    {"id_region": "02", "id_provincia": "023", "id_comuna": "02304", "nombre": "Paine"},

    {"id_region": "02", "id_provincia": "024", "id_comuna": "02301", "nombre": "Santiago"},
    {"id_region": "02", "id_provincia": "024", "id_comuna": "02302", "nombre": "Vitacura"},
    {"id_region": "02", "id_provincia": "024", "id_comuna": "02303", "nombre": "Renca"},
    {"id_region": "02", "id_provincia": "024", "id_comuna": "02304", "nombre": "Quinta Normal"},

    {"id_region": "03", "id_provincia": "031", "id_comuna": "03101", "nombre": "Quillota³"},
    {"id_region": "03", "id_provincia": "031", "id_comuna": "03102", "nombre": "La Calera"},
    {"id_region": "03", "id_provincia": "031", "id_comuna": "03103", "nombre": "La Cruz"},
    {"id_region": "03", "id_provincia": "031", "id_comuna": "03104", "nombre": "Nogales"},

    {"id_region": "03", "id_provincia": "032", "id_comuna": "03201", "nombre": "San Antonio"},
    {"id_region": "03", "id_provincia": "032", "id_comuna": "03202", "nombre": "Cartagena"},
    {"id_region": "03", "id_provincia": "032", "id_comuna": "03203", "nombre": "El Tabo"},
    {"id_region": "03", "id_provincia": "032", "id_comuna": "03204", "nombre": "El Quisco"},
    {"id_region": "03", "id_provincia": "032", "id_comuna": "03205", "nombre": "Algarrobo"},
    {"id_region": "03", "id_provincia": "032", "id_comuna": "03206", "nombre": "Santo Domingo"},

    {"id_region": "03", "id_provincia": "033", "id_comuna": "03301", "nombre": "San Felipe"},
    {"id_region": "03", "id_provincia": "033", "id_comuna": "03302", "nombre": "Llaillay"},
    {"id_region": "03", "id_provincia": "033", "id_comuna": "03303", "nombre": "Putaendo"},
    {"id_region": "03", "id_provincia": "033", "id_comuna": "03304", "nombre": "Santa Maria"},
    {"id_region": "03", "id_provincia": "033", "id_comuna": "03305", "nombre": "Catemu"},
    {"id_region": "03", "id_provincia": "033", "id_comuna": "03306", "nombre": "Panquehue"},

    {"id_region": "03", "id_provincia": "034", "id_comuna": "03301", "nombre": "Casa Blanca"},
    {"id_region": "03", "id_provincia": "034", "id_comuna": "03302", "nombre": "Quintero"},
    {"id_region": "03", "id_provincia": "034", "id_comuna": "03303", "nombre": "Con Con"},
    {"id_region": "03", "id_provincia": "034", "id_comuna": "03304", "nombre": "Valparaiso"},
    {"id_region": "03", "id_provincia": "034", "id_comuna": "03305", "nombre": "Viña del Mar"},
    {"id_region": "03", "id_provincia": "034", "id_comuna": "03306", "nombre": "Puchuncabi"},

    {"id_region": "04", "id_provincia": "041", "id_comuna": "04101", "nombre": "Rancagua"},
    {"id_region": "04", "id_provincia": "041", "id_comuna": "04102", "nombre": "Rengo"},

    {"id_region": "04", "id_provincia": "042", "id_comuna": "04201", "nombre": "Codegua"},
    {"id_region": "04", "id_provincia": "042", "id_comuna": "04202", "nombre": "Machali"},
]



