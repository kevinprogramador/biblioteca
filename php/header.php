<html lang="es-ES">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
        <title>Biblioteca</title>
        <nav class="navbar navbar-dark bg-dark">
            <div class="container">
                <label class="navbar-brand">Bienvenido Usuario: <?php echo $usuario->getNombre(); ?></label>                
            </div>
        </nav>
        <div class="container alto">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link active" href="inicio.php">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="mantenedor.php">Mantenedor Libros</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="solicitud.php">Solicitar Libro</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="devolver.php">Devolver Libro</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php">logout</a>
                </li>
            </ul>
        </head>