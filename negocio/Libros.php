<?php

namespace Negocio;

include 'bd.php';
include 'Entidades/Libros.php';

use Entidades as ent;

class Libros {

    private $bd;

    function __construct() {
        $this->bd = new BD();
    }

    //FUNCION QUE LISTA TODOS LOS DATOS
    public function obtenerListaLibros() {
        $result = $this->bd->select("SELECT id, nombrelibro, autor, editorial, anio, estado FROM libros");
        $libros = [];     
        foreach ($result as $libro) {
           
            array_push($libros, new ent\Libros($libro["id"],$libro["nombrelibro"],$libro["autor"],$libro["editorial"],$libro["anio"],$libro["estado"]));
        }
        return $libros;
    }

    //FUNCION QUE ELIMINA UN REGISTRO DEL CLIENTE DENTRO DE LA BASE DE DATOS
    public function eliminarLibro($id) {
        $SQL = "DELETE FROM libros WHERE id='$id'";
        return $this->bd->query($SQL);
    }

    //FUNCION QUE AGREGA UN REGISTRO DEL CLIENTE DENTRO DE LA BASE DE DATOS
    public function agregarLibro($libro) {
        $id = $libro->getId();
        $nombrelibro = $libro->getNombreLibro();
        $autor = $libro->getAutor();
        $editorial = $libro->getEditorial();
        $anio = $libro->getAnio();
        $estado = $libro->getEstado();
        $SQL = "INSERT INTO Libros (id, nombrelibro, autor, editorial, anio, estado) "
                . "VALUES ('$id', '$nombrelibro', '$autor', '$editorial', '$anio', '$estado')";

        return $this->bd->query($SQL);
    }

    //FUNCION QUE ACTUALIZA UN REGISTRO DEL CLIENTE DENTRO DE LA BASE DE DATOS
    public function actualizarDatosLibros($libro) {

        $id = $libro->getId();
        $nombrelibro = $libro->getNombreLibro();
        $autor = $libro->getAutor();
        $editorial = $libro->getEditorial();
        $anio = $libro->getAnio();
        $estado = $libro->getEstado();
        $SQL = "UPDATE Libros SET nombrelibro='$nombrelibro', autor='$autor', editorial='$editorial', anio='$anio', estado='$estado'"
                . "WHERE id='$id'";
        return $this->bd->query($SQL);
    }   

    public function solicitarLibro($id) {
        $SQL = "UPDATE libros SET ESTADO = 'prestado' WHERE id='$id'";
        return $this->bd->query($SQL);
    }
    
    public function devolverLibro($id) {
        $SQL = "UPDATE libros SET ESTADO = 'disponible' WHERE id='$id'";
        return $this->bd->query($SQL);
    }
}
?>

