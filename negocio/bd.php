<?php

namespace Negocio;

class BD {

    private $mysqli;

    public function __construct() {
        $this->mysqli = new \mysqli("localhost", "root", "", "biblioteca");
    }

    function query($q) {
        return $this->mysqli->query($q);
    }

    function select($q) {
        $rows = [];
        $result = $this->mysqli->query($q);

        if ($result->num_rows > 0) {
// output data of each row
            while ($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
        }
        return $rows;
    }

}

?>
