create database biblioteca;

use biblioteca;

create table libros
(
    id varchar(12) primary key,
    nombrelibro varchar(100),
    autor varchar(100),
    editorial varchar(100),
    anio date,
    estado varchar(100)
);

create table login
(
    usuario varchar(10) primary key,
    password varchar(20)
);


insert into login
values('kevin', 'kevin');


insert into libros
values('01', 'papelucho', 'Marcela Paz', 'zigzag', '1998-02-25', 'disponible');
insert into libros
values('02', 'sub terra', 'Marcela Paz', 'ercilla', '2005-08-20', 'disponible');
insert into libros
values('03', 'principito', 'Marcela Paz', 'ercilla', '1997-12-25', 'disponible');
insert into libros
values('04', 'poder psicotronico', 'Marcela Paz', 'zigzag', '1970-09-01', 'disponible');
insert into libros
values('05', 'ley de la atraccion', 'Marcela Paz', 'zigzag', '1998-08-24', 'disponible');
insert into libros
values('06', 'romeo y julieta', 'Marcela Paz', 'zigzag', '1991-07-15', 'disponible');
